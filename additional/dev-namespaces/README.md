## Additional Development Namespaces

1. Fill out the `dev_namespaces` section of [tap-install-config](../../tap-install-config.yml).

2. Deploy with:

```bash
ytt --data-values-file ../../tap-install-secrets.yml --data-values-file ../../tap-install-config.yml -f . | kubectl apply -f-
```